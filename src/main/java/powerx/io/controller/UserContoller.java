package powerx.io.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import powerx.io.model.User;
import powerx.io.service.UserService;

@Controller
public class UserContoller {

	public static Logger logger = LoggerFactory.getLogger("monitor");
	@Autowired
	private UserService userService;
	
	@ResponseBody
	@RequestMapping("/get")
	public Object getData(String userName) {
		logger.info("获取用户信息");
		return userService.findByName(userName);
	}
	
	@ResponseBody
	@RequestMapping("/insert")
	public Object insert(User user) {
		logger.info("添加用户");
		return userService.insertUser(user);
	}
}

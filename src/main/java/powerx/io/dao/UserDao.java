package powerx.io.dao;

import powerx.io.model.User;

public interface UserDao {

	User findByName(String userName);
	
	int insertUser(User user);
}

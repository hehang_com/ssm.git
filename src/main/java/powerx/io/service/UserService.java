package powerx.io.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import powerx.io.dao.UserDao;
import powerx.io.model.User;

@Service
public class UserService {
	
	@Autowired
	private UserDao userDao;
	public Object findByName(String userName) {
		return userDao.findByName(userName);
 	}
	
	@Transactional
	public int insertUser(User user) {
		//测试事务是否起作用
		userDao.insertUser(user);
		return userDao.insertUser(user);
 	}

}
